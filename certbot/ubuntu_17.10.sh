#!/bin/bash

sudo apt-get update 
sudo apt-get install software-properties-common -y
sudo add-apt-repository ppa:certbot/certbot -y
sudo apt-get update
sudo apt-get install python-certbot-nginx -y

sudo apt install docker.io -y

sudo chmod 777 /var/run/docker.sock

sudo certbot --nginx certonly

sudo certbot renew --dry-run