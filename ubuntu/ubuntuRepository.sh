#Alacarte
sudo apt install alacarte -y
sleep 3

#Arc-Theme
sudo apt install arc-theme -y
sleep 3

#Git
sudo apt install git -y 
sleep 3

#Flatpak
sudo apt install flatpak -y 
sleep 3

#Gparted
sudo apt install gparted -y
sleep 3

#Gpick
sudo apt install gpick -y
sleep 3

#Inkscape
sudo apt install inkscape -y 
sleep 3

#Kazam
sudo apt install kazam -y 
sleep 3

#Shutter
sudo apt install shutter -y 
sleep 3

#Synergy
sudo apt install synergy -y 
sleep 3

#Tree
sudo apt install tree -y 
sleep 3

#Uget
sudo apt install uget -y 
sleep 3

#Vim
sudo apt install vim -y 
sleep 3

#Arduino
sudo apt install arduino -y 
sleep 3

# GNOME Shell integration for Chrome
sudo apt install chrome-gnome-shell -y
sleep 3

# Curl
sudo apt install curl -y
sleep 3

#Pip
sudo apt install python-pip -y
sleep 3

#FFmpeg
sudo apt install ffmpeg -y
sleep 3

#Terminator
sudo apt install terminator -y
sleep 3

#Gnome Tweak Tool
sudo apt install gnome-tweak-tool -y
sleep 3

#Google Cloud SDK
sudo apt install google-cloud-sdk -y
sleep 3

#MySQL WorkBench
sudo apt install mysql-workbench -y
sleep 3
