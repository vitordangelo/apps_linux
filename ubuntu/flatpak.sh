#Gimp
sudo flatpak install https://flathub.org/repo/appstream/org.gimp.GIMP.flatpakref -y
sleep 3

#Peek
sudo flatpak install flathub com.uploadedlobster.peek -y
sleep 3

#Skype
sudo flatpak install flathub com.skype.Client -y
sleep 3

#Gradio
flatpak install flathub de.haeckerfelix.gradio
sleep 3
