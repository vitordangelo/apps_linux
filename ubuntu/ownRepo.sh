#NodeJS 9
sudo apt remove nodejs -y
sudo curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash - 
sudo apt-get install nodejs -y
sleep 3

#Steam
wget http://repo.steampowered.com/steam/archive/precise/steam_latest.deb 
sudo dpkg -i steam_latest.deb 
sudo apt-get install -f 
sleep 3

#Docker Compose
sudo curl -L https://github.com/docker/compose/releases/download/1.19.0/docker-compose-`uname -s`-`uname -m` -o /usr/bin/docker-compose
sleep 3

# Fishshell
curl -L https://get.oh-my.fish | fish
fish 
omf install robbyrussell
sudo chsh -s /usr/bin/fish
sleep 3

# youtube-dl
sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/bin/youtube-dl
sudo chmod a+rx /usr/bin/youtube-dl
sleep 3