#Fish
sudo apt-add-repository ppa:fish-shell/release-2 -y
sudo apt-get update 
sudo apt-get install fish -y 
sleep 3

#Papirus Icon
sudo add-apt-repository ppa:papirus/papirus -y 
sudo apt-get update 
sudo apt-get install papirus-icon-theme -y
sleep 3

#Y PPA
sudo add-apt-repository ppa:webupd8team/y-ppa-manager -y 
sudo apt-get update 
sudo apt install y-ppa-manager -y 
sleep 3

#Java 9
sudo add-apt-repository ppa:webupd8team/java -y 
sudo apt-get update 
sudo apt install oracle-java9-installer -y 