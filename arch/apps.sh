#!/bin/bash

#Albert
yaourt -S albert --noconfirm

#Gparted
yaourt -S gparted --noconfirm

#Gpick
yaourt -S gpick --noconfirm

#Inkscape
yaourt -S inkscape --noconfirm

#Kazam
yaourt -S kazam --noconfirm

#Shutter
yaourt -S shutter --noconfirm

#Synergy
yaourt -S synergy --noconfirm

#Tree
yaourt -S tree --noconfirm

#Uget
yaourt -S uget --noconfirm

#Vim
yaourt -S vim --noconfirm

#Arduino
yaourt -S arduino --noconfirm

# GNOME Shell integration for Chrome
yaourt -S chrome-gnome-shell --noconfirm

#Curl
yaourt -S curl --noconfirm

#Pip
yaourt -S python-pip --noconfirm

#FFmpeg
yaourt -S ffmpeg --noconfirm

#Chrome
yaourt -S google-chrome --noconfirm

#Insomnia
yaourt -S insomnia --noconfirm

#Docker
yaourt -S docker --noconfirm

#Docker Compose
yaourt -S docker-compose --noconfirm

#Fish
yaourt -S fish --noconfirm

#VS Code
yaourt -S code --noconfirm

#NodeJS NPM
yaourt -S nodejs  npm --noconfirm

#Steam
yaourt -S steam --noconfirm

#Youtube-dl
yaourt -S youtube-dl --noconfirm

#Papirus Icon
sudo pacman -S papirus-icon-theme

#Java
yaourt -S java-environment=8 --noconfirm

#Spotify
yaourt -S spotify --noconfirm

#VLC
yaourt -S vlc --noconfirm

#Skype
yaourt -S skype --noconfirm

#Angular CLI
sudo npm install -g @angular/cli

#Nodemon
sudo npm install -g 

#Telegran
yaourt -S  telegram-desktop-bin --noconfirm